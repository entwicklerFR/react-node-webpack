import React from "react";
import PropTypes from 'prop-types';

export default class Clock extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            time: this.time()
        };
    }

    time() {
        let options = {
            hourCycle: 'h24',
            year: 'numeric', month: 'numeric', day: 'numeric',
            hour: 'numeric', minute: 'numeric', second: 'numeric',
            timeZone: this.props.timeZone
        };

        try {
            let today = new Date();
            return today.toLocaleDateString([], options);
        }
        catch (e) {
            return e.message;
        }
    }

    tick() {
        this.setState({
            time: this.time()
        });
    }

    componentDidMount() {
        this.intervalID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.intervalID);
    }

    render() {
        return (
            <div className="item">
                <div>{(this.props.children) ? this.props.children : this.props.timeZone}</div>
                <div>{(this.props.time) ? this.props.time : this.state.time}</div>
            </div>
        );

    }

}

Clock.defaultProps = {
    timeZone: "Europe/Paris"
};