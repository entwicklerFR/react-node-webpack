import React from "react";
import Clock from './Clock';

import './App.css';

export default class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = { 'data': {} };
  }

  getRemoteTime(timeZone) {
    let data = {};
    let xhr = new XMLHttpRequest();

    xhr.open('GET', encodeURI('/api/date?timezone='.concat(timeZone)));
    xhr.responseType = 'json';
    xhr.onload = () => {
      if (xhr.status === 200) { data = xhr.response; }
      this.setState({ 'data': data });
    };

    xhr.send();
  }

  componentDidMount() {
    this.timer = setInterval(() => this.getRemoteTime("America/New_York"), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  render() {

    return (
      <div className="container">
        <div className="header item">
          <Clock />
          <Clock><b>Lyon</b></Clock>
          <Clock time={new Date().toLocaleDateString([],
            {
              hourCycle: 'h24', year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric', timeZone: "Europe/Paris"
            })}><i>Fixed/Init_Time</i></Clock>
          <Clock timeZone="Europe/London" />
          <Clock timeZone="Europe/Berlin" />
          <Clock timeZone="America/New_York" />
          <Clock timeZone="Asia/Tokyo" />
          <Clock timeZone="Asia/Unknown_City" />
        </div>
        <div className="content">
          <div className="nav item">React App</div>
          <div className="display item">
            Display area          
            <Clock time={this.state.data.localeDateString}>Remote time for {this.state.data.timeZone}</Clock>
          </div>
        </div>
        <div className="footer item">&copy; Ent Wickler</div>
      </div>
    );
  }

}