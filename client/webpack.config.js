var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  mode: 'none',

  entry: [
    path.resolve(__dirname, "./index.jsx")
  ],

  output: {
    path: path.resolve(__dirname, "./dist"),
    filename: "bundle.js"
  },

  plugins: [
    new HtmlWebpackPlugin({
      title: 'React App with Node Backend',
      filename: 'index.html',
      template: 'index.htm'
    })
  ],

  devServer: {
    proxy: {
      "/api": "http://localhost:3000"
    }
  },

  module: {

    rules: [
      /* both JS and JSX files are processed so I could name a JSX file with ".js" suffix*/
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env', 'react']
          }
        }
      },

      /* css */
      {
        test: /\.css$/,
        use: [{
            loader: "style-loader"
          },
          {
            loader: "css-loader"
          }
        ]
      }

    ]

  },

  /* our './myComponent' requires or imports are equivalent to 'myComponent.jsx' */
  resolve: {
    extensions: ['*', '.js', '.jsx']
  }
}