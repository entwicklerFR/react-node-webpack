# React App with Node Backend
### Development Environment Setup with Webpack
The purpose of this project is to show how I am used to configuring my development environment when I need to create a React App based on a Node Backend which is simultaneously an API and a Web Server.  

For more details, I suggest you to read this [post][0] on my blog.

[0]:https://entwicklerfr.gitlab.io/2018/05/react-app-with-node-backend/