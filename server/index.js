'use strict';

var express = require('express');
var app = express();
app.use(express.urlencoded({'extended': true}));
var apiRoutes = express.Router();
app.use('/api', apiRoutes);

/*
http://localhost:3000/api/date?timezone=America/New_York 
return : {"localeDateString":"2018-5-5 16:07:41"}
*/
apiRoutes.get('/date', function (req, res) {
    let options = {
        hourCycle: 'h24',
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric',
        timeZone: req.query.timezone
    };
    let result;

    try {
        let today = new Date();
        result = today.toLocaleDateString([], options);
    } catch (e) {
        result = e.message;
    }

    res.status(200).json({
        'localeDateString': result,
        'timeZone': req.query.timezone
    });
    return;
});

app.listen(3000, function () {
    console.log('Node Backend started on port [%d]', 3000);
});